Hooks.once('dragRuler.ready', (SpeedProvider) => {

  class ShadowdarkSpeedProvider extends SpeedProvider {
    get colors() {
      return [
        {id: 'walk', default: 0x00FF00, name: 'shadowdark.speeds.walk'},
        {id: 'dash', default: 0xFFFF00, name: 'shadowdark.speeds.dash'}
      ]
    }

    getRanges (token) {
      const move = token.actor.system.move ?? 'near'

      const moveTranslation = {
        close: 5,
        near: 30,
        doubleNear: 60,
        tripleNear: 90,
        far: 120
      }

      const baseSpeed = moveTranslation[move] ?? 30

      const ranges = [
        {range: baseSpeed, color: 'walk'},
        {range: baseSpeed * 2, color: 'dash'}
      ]

      return ranges
    }
  }

  dragRuler.registerModule('drag-ruler-integration-for-shadowdark', ShadowdarkSpeedProvider)
  
})
