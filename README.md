# Shadowdark Drag-Ruler Integration

This adds [drag-ruler](https://github.com/manuelVo/foundryvtt-drag-ruler/tree/master) support for the Shadowdark system.

Despite the intended vagueness of Shadowdark's unique movement system, this translates actor move values into numbers. Specifically, "close" as 5 ft, "near" as 30 ft, "double near" as 60 ft, and "far" as 120 ft.
